﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Events : MonoBehaviour
    {
        /// <summary>
        /// Очередь Shapes на проверку совпадений
        /// </summary>
        public List<GameObject> CheckMatchesQueue = new List<GameObject>();

        [Tooltip("Ссылка на объект Field")]
        public Field Field;
        [Tooltip("Периодичность проверок. Влияет на скорость игры")]
        public float CheckTimer = 0.5f;

        private float _timer;
        private bool _fillVoids;

        public bool FillVoids {
            get { return _fillVoids; }
            set 
            {
                if (value) _timer = CheckTimer;                                         // Сброс таймера
                _fillVoids = value;
            }
        }

        private void Update()
        {
            Timer();
        }

        private void Timer()
        {
            _timer -= Time.deltaTime;

            if (_timer <= 0)
            {
                _timer = CheckTimer;
                Tests();
            }
        }

        private void Tests()
        {
            // Запуск проверки совпадений для каждого объекта в очереди
            if (CheckMatchesQueue.Count > 0)
            {
                foreach (GameObject shape in CheckMatchesQueue)
                {
                    Utilities.CheckMatches(Field, shape);
                }

                CheckMatchesQueue.Clear();
            }
            // Заполнение образовавшихся пустот после совпадения
            else if (FillVoids)
            {
                Field.FillVoids();
                FillVoids = false;
            }
            // Разрешение управления игроку
            else if(!PlayerController.Swapping)
            {
                PlayerController.SelectPermission = true;
            }
        }
    }
}
