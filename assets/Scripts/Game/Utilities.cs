﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Utilities
    {
        public delegate void WaitDelegate();

        /// <summary>
        /// Проверка совпадений с переданным Shape
        /// </summary>
        /// <returns>Имеется ли совпадение</returns>
        public static bool CheckMatches(Field field, GameObject shape)//, bool fillVoids = false)
        {
            if (shape == null) return false;

            // Получение совпадений по горизонтали и вертикали
            GameObject[] horizontalMatches = GetHorizontalMathces(field, shape);
            GameObject[] verticalMatches   = GetVerticalMathces  (field, shape);

            // Выход если совпадений по каждой оси меньше минимального
            if (horizontalMatches.Length < field.MinimumMatches- 1 && verticalMatches.Length < field.MinimumMatches - 1)
            {
                return false;
            }

            // Уничтожение совпавших Shapes если они удовлетворяют минимальному количеству
            if (horizontalMatches.Length >= field.MinimumMatches - 1)                   // -1 ибо в массивах нет сходного Shape
            {
                foreach (GameObject match in horizontalMatches)
                {
                    field.DestroyShape(match);
                }
            }

            if (verticalMatches.Length >= field.MinimumMatches - 1)
            {
                foreach (GameObject match in verticalMatches)
                {
                    field.DestroyShape(match);
                }
            }

            field.DestroyShape(shape);                                                  // Уничтожение исходного Shape
            field.Events.FillVoids = true;                                              // Создаем необходимость заполнить образовавшиеся пустоты для таймера

            // Рассчет количества совпадений
            int matches = 1;
            matches += (horizontalMatches.Length >= field.MinimumMatches - 1) ? horizontalMatches.Length : 0;
            matches += (verticalMatches.Length   >= field.MinimumMatches - 1) ? verticalMatches.Length   : 0;

            // Расчет получаемых очков и их прибавка
            ScoreManager sManager = field.ScoreManager;
            sManager.AddScore(CalculateScore(matches, field.MinimumMatches, sManager.Match3Score, sManager.BonusScore));

            return true;
        }

        /// <summary>
        /// Расчет количества получаемых очков от совпадений
        /// </summary>
        /// <param name="matches">Количество совпадений</param>
        /// <param name="minMatches">Минимальное количество очков для получение </param>
        /// <param name="match3Score">Очки за minMatches</param>
        /// <param name="bonusScore">Бонусные очки за каждое последующее совпадение после minMatches</param>
        /// <returns>Возвращает количество получаемых очков</returns>
        public static int CalculateScore(int matches, int minMatches, int match3Score, int bonusScore)
        {
            // Выход если совпадений меньше необходимого
            if (matches < minMatches) return 0;

            int score = match3Score;                                                    // Очки за минимальное количество совпаденией
            matches -= minMatches;
            // Выход если нет бонусных совпадений
            if (matches <= 0) return score;

            score += matches * bonusScore;                                              // Прибавка бонусных очков
            return score;
        }

        /// <summary>
        /// Получить совпадения Shape по горизонтали
        /// </summary>
        /// <param name="shape">Исходный Shape</param>
        /// <returns>Возвращает массив совпадений</returns>
        private static GameObject[] GetHorizontalMathces(Field field, GameObject shape)
        {
            Vector2 sPos = shape.transform.position;                                    // Получение позиции исходного Shape
            List<GameObject> matches = new List<GameObject>();                          // Создание списка совпадений

            int baseX = (int)sPos.x;                                                    // Перевод позиции в int
            int baseY = (int)sPos.y;

            int offset = 0;                                                             // Инициализация смещения

            // Цикл проверяет Shapes от исходного влево. При совпадении добавляет в список. Иначе цикл прерывается
            while (true) { if (!CheckMatch(baseX - ++offset, baseY, field, shape, matches)) break; }

            // Аналогичная проверка, но вправо
            offset = 0;
            while (true) { if (!CheckMatch(baseX + ++offset, baseY, field, shape, matches)) break; }

            return matches.ToArray();
        }

        /// <summary>
        /// Получить совпадения Shape по вертикали
        /// </summary>
        /// <param name="shape">Исходный Shape</param>
        /// <returns>Возвращает массив совпадений</returns>
        private static GameObject[] GetVerticalMathces(Field field, GameObject shape)
        {
            Vector2 sPos = shape.transform.position;                                    // Получение позиции исходного Shape
            List<GameObject> matches = new List<GameObject>();                          // Создание списка совпадений

            int baseX = (int)sPos.x;                                                    // Перевод позиции в int
            int baseY = (int)sPos.y;

            int offset = 0;                                                             // Инициализация смещения

            // Цикл проверяет Shapes от исходного вверх. При совпадении добавляет в список. Иначе цикл прерывается
            while (true) { if (!CheckMatch(baseX, baseY + ++offset, field, shape, matches)) break; }

            // Вниз
            offset = 0;
            while (true) { if (!CheckMatch(baseX, baseY - ++offset, field, shape, matches)) break; }

            return matches.ToArray();
        }

        /// <summary>
        /// Проверка shape на совпадение c sourceShape
        /// </summary>
        /// <param name="x">Х позиция ячейки</param>
        /// <param name="y">Y позиция ячейки</param>
        /// <param name="sourceShape">Исходный Shape с которым необходимо сравнить</param>
        /// <param name="matches">Список совпадений в который будет произведена запись при совпадении</param>
        /// <returns>Есть ли совпадение</returns>
        private static bool CheckMatch(int x, int y, Field field, GameObject sourceShape, List<GameObject> matches)
        {
            GameObject shape = field.GetShape(x, y);                                    // Получение Shape в проверочной позиции
            if (shape == null || shape.name != sourceShape.name) return false;          // Выход если null или не совпадает имя
            matches.Add(shape);                                                         // Иначе добавляем в список совпадений
            return true;
        }

        /// <summary>
        /// Вызов метода с задержкой delay
        /// </summary>
        /// <param name="coroutineStarter">MonoBehavior от имени которого будет запускаться корутина</param>
        public static void Wait(float delay, WaitDelegate del, MonoBehaviour coroutineStarter)
        {
            coroutineStarter.StartCoroutine(WaitCoroutine(delay, del));
        }

        private static IEnumerator WaitCoroutine(float time, WaitDelegate wDelegate)
        {
            yield return new WaitForSeconds(time);
            wDelegate();
        }
    }
}
