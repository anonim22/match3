﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class PlayerController : MonoBehaviour
    {
        public static bool SelectPermission = true;                                     // Разрешение на управление
        public static bool Swapping;                                                    // Активность обмена местами двух Shapes

        public Field Field;
        [Tooltip("Скорость обмена местами Shapes")]
        public float SwapSpeed = 4f;
        [Tooltip("Задержка перед проверкой совпадений после обмена местами")]
        public float DelayAfterSwap = 0.25f;

        private GameObject _selectedShape;

        private void Update()
        {
#if UNITY_STANDALONE
            StandaloneControl();
#elif UNITY_ANDROID || UNITY_IOS
            MobileControl();
#endif
        }

        /// <summary>
        /// Полный рестарт игры
        /// </summary>
        // Знаю, можно и через загрузку сцены, но этот вариант, особенно в таком проекте более быстр и энергоэффективен
        public void RestartGame()
        {
            SelectPermission = true;
            StopAllCoroutines();
            Swapping = false;

            Field.Events.CheckMatchesQueue.Clear();
            Field.Events.FillVoids = false;
            Field.StopAllCoroutines();
            Field.DestroyGhostShapes();
            Field.RegenerateField();
        }

        /// <summary>
        /// Управление для PC
        /// </summary>
        private void StandaloneControl()
        {
            if (SelectPermission && Input.GetMouseButtonDown(0))
            {
                // Выход если был совершен клик по какому либо элементу UI
                if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                    return;

                CheckClick(Input.mousePosition);
            }
        }

        /// <summary>
        /// Управление для мобильных систем
        /// </summary>
        private void MobileControl()
        {
            if (SelectPermission && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                // Выход если был совершен тап по какому либо элементу UI
                if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                    return;

                CheckClick(Input.GetTouch(0).position);
            }
        }

        /// <summary>
        /// Проверка клика
        /// </summary>
        private void CheckClick(Vector2 clickScreenPosition)
        {
            Vector2 worldPos = Camera.main.ScreenToWorldPoint(clickScreenPosition);     // Преобразование позиции клика по экрану в мировые координаты

            // Выход если клик был за пределом игрового поля
            if(worldPos.x < 0 || worldPos.x >= Field.Column ||
               worldPos.y < 0 || worldPos.y >= Field.Row) return;

            // Преобразование в Int
            int x = (int) Mathf.Round(worldPos.x);
            int y = (int) Mathf.Round(worldPos.y);

            GameObject clickedShape = Field.GetShape(x, y);                             // Получение объекта Shape в месте клика

            // Выход если Shape не был получен
            if (clickedShape == null) return;

            // Если на данный момент не выбран ни один Shape
            if (_selectedShape == null)
            {
                SelectShape(clickedShape);                                              // Выбор Shape
                return;
            }

            // Если был произведен клик по выбранному Shape, то выделение снимается
            if (clickedShape == _selectedShape)
            {
                SelectShape(null);
                return;
            }

            // Если уже имеется выбранный Shape и дистанция между ним и текущим кликом больше одной клетки
            if(Vector2.Distance(_selectedShape.transform.position, new Vector2(x, y)) > 1)
            {
                SelectShape(clickedShape);                                              // Снимается выделение с первой клетки и переносится на вторую
                return;
            }

            // Обмен если выбранная клетка является соседней для кликнутой (Distance <= 1)
            Swap(_selectedShape, clickedShape, true);
            SelectShape(null);                                                          // Снимается выбор Shape
            SelectPermission = false;                                                   // Отключается возможность выбора Shapes
        }

        /// <summary>
        /// Выбор Shape
        /// </summary>
        /// <param name="shape"></param>
        private void SelectShape(GameObject shape)
        {
            // С текущего Shape снимается выделение
            if(_selectedShape) _selectedShape.GetComponent<SpriteRenderer>().color = Color.white;

            // А новый становится темнее на фоне остальных (выделение)
            if(shape != null) shape.GetComponent<SpriteRenderer>().color = new Color(0.75f, 0.75f, 0.75f);
            _selectedShape = shape;
        }

        /// <summary>
        /// Поменять два Shapes местами
        /// </summary>
        /// <param name="checkAfterSwap">Необходима ли проверка совпадений после обмена местами</param>
        private void Swap(GameObject shape1, GameObject shape2, bool checkAfterSwap)
        {
            StartCoroutine(SwapAnimation(shape1, shape2, checkAfterSwap));
        }

        /// <summary>
        /// Анимация обмена Shapes местами
        /// </summary>
        private IEnumerator SwapAnimation(GameObject shape1, GameObject shape2, bool checkAfterSwap)
        {
            Swapping = true;

            shape1.GetComponent<SpriteRenderer>().sortingOrder = 1;

            Transform transform1 = shape1.transform;
            Transform transform2 = shape2.transform;

            // Исходная позиция обоих Shapes
            Vector3 pos1 = transform1.position;
            Vector3 pos2 = transform2.position;

            // Необходимое направление движения для обоих Shapes
            Vector3 directrion1 = (pos2 - pos1) * SwapSpeed;
            Vector3 directrion2 = (pos1 - pos2) * SwapSpeed;

            // Обмен местами в массиве
            int x = (int)Mathf.Round(transform2.position.x);
            int y = (int)Mathf.Round(transform2.position.y);

            Field.SetShape(x, y, shape1);

            x = (int)Mathf.Round(transform1.position.x);
            y = (int)Mathf.Round(transform1.position.y);

            Field.SetShape(x, y, shape2);

            // Перемещение Shapes в мировом пространстве
            while (Vector3.Distance(transform1.position, pos2) > Time.deltaTime * SwapSpeed)
            {
                transform1.Translate(directrion1 * Time.deltaTime);
                transform2.Translate(directrion2 * Time.deltaTime);

                yield return null;
            }

            shape1.GetComponent<SpriteRenderer>().sortingOrder = 0;

            // Корректировка конечной позиции
            transform1.position = pos2;
            transform2.position = pos1;

            if (checkAfterSwap)
            {
                yield return new WaitForSeconds(DelayAfterSwap);

                bool m1 = Utilities.CheckMatches(Field, shape1);
                bool m2 = Utilities.CheckMatches(Field, shape2);

                if (m1 || m2)
                {
                    SelectShape(null);
                }
                else
                {
                    Swap(shape2, shape1, false);
                }
            }

            Swapping = false;
        }
    }
}