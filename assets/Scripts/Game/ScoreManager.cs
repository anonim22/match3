﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class ScoreManager : MonoBehaviour
    {
        [Tooltip("Ссылка на текст для отображения очков")]
        public Text Text;
        public int Score;

        [Header("Scores")]
        [Tooltip("Очки за 3 в ряд")]
        public int Match3Score = 10;
        [Tooltip("Бонусные очки за каждое последующее совпадение")]
        public int BonusScore = 5;

        /// <summary>
        /// Прибавить очки
        /// </summary>
        public void AddScore(int score)
        {
            Score += score;
            Text.text = Score.ToString();
        }
    }
}
