﻿using System.Collections;
using System.Collections.Generic;
using GameSystems.Pool;
using UnityEngine;

namespace Game
{
    public class Field : MonoBehaviour
    {
        [Tooltip("Колонки игрового поля")]
        public int Column = 6;
        [Tooltip("Ряды игрового поля")]
        public int Row = 6;
        [Tooltip("Количество Shapes для активации совпадения")]
        public int MinimumMatches = 3;
        [Tooltip("Скорость передвижения Shape на освободившееся место")]
        public float MovingSpeed = 4f;
        [Tooltip("Задержка перед уничтожением Shape")]
        public float DelayBeforeDestroyShape = 0.5f;
        [Tooltip("Массим фишек на игровом поле")]
        public GameObject[] Shapes;

        /// <summary>
        /// Массив со всеми фишками
        /// </summary>
        private GameObject[,] _field;

        public Events Events;
        public ScoreManager ScoreManager;

        // Shapes которые во время уничтожения некоторое время висят как призраки. Их нет ни в пуле, ни в массиве поля и по
        // этому приходится держать их в списке, чтобы уничтожить если во время такого состояния начнется перезапуск игры.
        private List<GameObject> _destroyingShapes = new List<GameObject>();

        // Use this for initialization
        void Start ()
	    {
            InitializeField();
	        InitializePool();
	        GenerateShapesField();
	    }

        /// <summary>
        /// Повторная генерация ShapeField
        /// </summary>
        public void RegenerateField()
        {
            // Перемещение всех активных Shape в пул
            foreach (var shape in _field)
            {
                if(!shape) continue;
                PoolManager.PutObject(shape);
            }

            InitializeField();                                                          // Повторное создание массива поля
            GenerateShapesField();                                                      // Генерация игрового поля
        }

        /// <summary>
        /// Генерация Shape на игровом поле
        /// </summary>
        private void GenerateShapesField()
        {
            for (int y = 0; y < Row; y++)
            {
                for (int x = 0; x < Column; x++)
                {
                    if(GetShape(x, y)) continue;
                    PlaceRandomShape(x, y);
                }
            }
        }

        /// <summary>
        /// Создание случайного Shape с учетом соседних клеток
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        private void PlaceRandomShape(int x, int y)
        {
            List<int> identifiers = new List<int>();                            // Массив идентификаторов объектов
            for (int i = 0; i < Shapes.Length; i++) identifiers.Add(i);         // Заполнение массива идентификаторами

            // Случайный перебор всех вариаций объектов
            while (identifiers.Count > 0)
            {
                int rIdentifier = Random.Range(0, identifiers.Count);           // Выбор случайного объекта из массива Shapes
                string objName = Shapes[identifiers[rIdentifier]].name;

                // Есть ли возможность разместить выбранный Shape не допустив комбинации из 3-х в ряд
                if (CanPlaceShape(objName, x, y))
                {
                    CreateShape(x, y, objName);                                 // Создание Shape 
                    break;
                }

                identifiers.RemoveAt(rIdentifier);                              // Удаление выбранного идентификатора
            }

            // Если вокруг уже имеется все 4 различных варианта Shape, то генерируется случайный и выполняется проверка совпадений 
            if (identifiers.Count == 0)
            {
                GameObject shape = CreateShape(x, y, Shapes[Random.Range(0, Shapes.Length)].name);
                Events.CheckMatchesQueue.Add(shape);
            }

        }

        /// <summary>
        /// Проверка на возможность разместить Shape не допустив комбинации из 3-х в ряд
        /// </summary>
        /// <param name="sourceName">Имя объекта который требуется разместить</param>
        /// <returns>Разрешение на установку переданного Shape</returns>
        private bool CanPlaceShape(string sourceName, int x, int y)
        {
            // Смещение для всех соседних ячеек
            Vector2[] neighboringCells = {
                Vector2.left, Vector2.right, Vector2.up, Vector2.down
            };

            // Проверка соседних ячеек
            foreach (Vector2 offset in neighboringCells)
            {
                int checkX = (int) (x + offset.x);
                int checky = (int) (y + offset.y);

                // Возвращает false если в соседней ячейке имеется Shape с данным именем
                GameObject neighbor = GetShape(checkX, checky);
                if (neighbor != null && neighbor.name == sourceName)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Перемещение нижних Shapes на освободившееся место
        /// </summary>
        public void FillVoids()
        {
            List<List<int>> emptyCells = new List<List<int>>();                         // Список пустых клеток

            // Проверка всего игрового поля
            for (int x = 0; x < Column; x++)
            {
                emptyCells.Add(new List<int>());
                for (int y = Row - 1; y >= 0; y--)
                {
                    GameObject shape = GetShape(x, y);
                    // Если клетка не пустая
                    if (shape != null)
                    {
                        //Проверка на наличие пустой клетки выше в данной колонке
                        if (emptyCells[x].Count > 0)
                        {
                            // Перемещение Shape в пустое место
                            StartCoroutine(MoveShape(shape, new Vector2(x, emptyCells[x][0])));

                            SetShape(x, y, null);
                            SetShape(x, emptyCells[x][0], shape);

                            emptyCells[x].RemoveAt(0);                                  // Клетку (x, emptyCells[x][0]) уже занимает shape. Она не нужна
                            emptyCells[x].Add(y);                                       // А текущая клетка остается пустой (с нее уехал Shape)
                        }
                        continue;
                    }
                    // Если клетка пустая, то добавляем ее в список
                    emptyCells[x].Add(y);
                }
            }

            Utilities.Wait(1f / (MovingSpeed - 1), GenerateShapesField, this);          // На месте перемещенных Shapes с задержкой создаются новые
        }

        /// <summary>
        /// Перемещение Shape на новую позицию
        /// </summary>
        /// <param name="shape"></param>
        /// <param name="destinationPosition"></param>
        /// <returns></returns>
        private IEnumerator MoveShape(GameObject shape, Vector3 destinationPosition)
        {
            Transform sTransform = shape.transform;

            Vector3 direction = destinationPosition - sTransform.position;

            // Перемещение в сторону новой позиции пока не достигнет минимального расстояния
            while (Vector3.Distance(sTransform.position, destinationPosition) > direction.y * Time.deltaTime * MovingSpeed)
            {
                sTransform.Translate(direction * Time.deltaTime * MovingSpeed);
                yield return null;
            }

            sTransform.position = destinationPosition;                                  // Коррекция позиции
            Events.CheckMatchesQueue.Add(shape);                                        // Ставим Shape в очередь на проверку совпадений
        }

        /// <summary>
        /// Создание объекта Shape и его размещение в мировом пространстве
        /// </summary>
        /// <param name="shapeName">Имя Shape</param>
        /// <returns>Возвращает созданный Shape</returns>
        private GameObject CreateShape(int x, int y, string shapeName)
        {
            GameObject shape = PoolManager.GetObject(shapeName);
            shape.transform.position = new Vector3(x, y, 0);
            SetShape(x, y, shape);
            return shape;
        }

        /// <summary>
        /// Уничтожение Shape с игрового поля
        /// </summary>
        /// <param name="shape"></param>
        public void DestroyShape(GameObject shape)
        {
            StartCoroutine(DestroyShapeCoroutine(shape));
        }

        private IEnumerator DestroyShapeCoroutine(GameObject shape)
        {
            _destroyingShapes.Add(shape);
            Vector2 mPos = shape.transform.position;

            int x = (int)Mathf.Round(mPos.x);
            int y = (int)Mathf.Round(mPos.y);

            SetShape(x, y, null);
            shape.GetComponent<SpriteRenderer>().color = new Color(0.75f,0.75f,0.75f);

            yield return new WaitForSeconds(DelayBeforeDestroyShape);

            shape.GetComponent<SpriteRenderer>().color = Color.white;
            _destroyingShapes.Remove(shape);
            PoolManager.PutObject(shape);
        }

        /// <summary>
        /// Уничтожение Shapes, которые остаются существовать если перезапустить игру во время их уничтожения
        /// </summary>
        public void DestroyGhostShapes()
        {
            foreach (GameObject shape in _destroyingShapes)
            {
                shape.GetComponent<SpriteRenderer>().color = Color.white;
                PoolManager.PutObject(shape);
            }

            _destroyingShapes.Clear();
        }

        /// <summary>
        /// Получение объекта с поля
        /// </summary>
        public GameObject GetShape(int x, int y)
        {
            // Возвращение null если выход за границы поля
            if (x < 0 || y < 0 || x >= _field.GetLength(0) || y >= _field.GetLength(1))
                return null;

            return _field[x, y];
        }

        /// <summary>
        /// Установка Shape в массиве
        /// </summary>
        public void SetShape(int x, int y, GameObject shape)
        {
            if (x < 0 || y < 0 || x >= _field.GetLength(0) || y >= _field.GetLength(1))
                return;

            _field[x, y] = shape;
        }

        /// <summary>
        /// Создание массива игрового поля для Shapes 
        /// </summary>
        private void InitializeField()
        {
            _field = new GameObject[Column, Row];
        }

        /// <summary>
        /// Создание пула объектов
        /// </summary>
        private void InitializePool()
        {
            foreach (var shape in Shapes)
            {
                PoolManager.CreatePool(shape);
            }
        }
    }
}