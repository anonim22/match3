﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace GameSystems.Pool
{
    public class PoolManager {
        private static List<Pool> _pools = new List<Pool>();
        private static Transform _objectsParent;

        /// <summary>
        /// Проверка существование родительского transform
        /// </summary>
        private static void CheckParentTransform()
        {
            if (_objectsParent == null)
            {
                _objectsParent = new GameObject("PoolObjects").transform;
                Object.DontDestroyOnLoad(_objectsParent);
            }
        }

        /// <summary>
        /// Создание пула объектов
        /// </summary>
        /// <param name="originalGameObject">Ссылка на неизменяебый GameObject (префаб)</param>
        /// <param name="maxObjects">Максимальное количество объектов в пуле (0 - неограниченное)</param>
        public static Pool CreatePool(GameObject originalGameObject, int maxObjects = 0)
        {
            CheckParentTransform();                                             // Создание родительского контейнера если еще не создан

            // Проверка существование объекта
            if (!originalGameObject)
            {
                Debug.LogError("GameObject cannot be null");
                throw new NullReferenceException();
            }

            Pool? pool = GetPool(originalGameObject.name);                      // Получение пула c именем объекта

            // Проверка результата.
            if (pool.HasValue)
            {
                throw new PoolAlreadyCreated();                                 // Бросаем исключение если пул с данным именем уже существует
            }

            Pool newPool = new Pool(originalGameObject, maxObjects);            // Создание пула объектов
            _pools.Add(newPool);                                                // Добавление в пул
            return newPool;                                                     // Возвращение созданного пула
        }

        /// <summary>
        /// Добавить объект в пул
        /// </summary>
        /// <param name="gameObject">Ссылка на передаваемый GameObject</param>
        public static void PutObject(GameObject gameObject)                     
        {
            Pool? nullablePool = GetPool(gameObject.name);                      // Получение nullable пула
            if (!nullablePool.HasValue)                                         // Проверка его существования
            {                                  
                Debug.LogError("Can't find pool for GameObject " + gameObject.name);
                return;
            }
            Pool pool = nullablePool.Value;                                     // Получение чистого пула (структуру, не nullable)

            // Выход если пул полон
            if (pool.MaxObjects > 0 && pool.Count >= pool.MaxObjects)
            {
                Debug.LogError("Pool " + gameObject.name + " is full");
                return;
            }

            // Проверка пула на отсутствие ссылки на этот же объект
            if (pool.UnusedObjects.Contains(gameObject))
            {
                Debug.LogError("Pool already contain this object: " + gameObject.name);
                return;
            }          

            gameObject.SetActive(false);                                        // Отключаение объекта
            gameObject.transform.parent = pool.Container;                          // Присваивание объекту родителя
            pool.UnusedObjects.Add(gameObject);                                 // Добавление объекта в список неиспользуемых        
            pool.UsedObjects.Remove(gameObject);                                // Убираем его из списка используемых
        }

        /// <summary>
        /// Получение объекта из пула
        /// </summary>
        /// <param name="name">Имя объекта</param>
        public static GameObject GetObject(string name)
        {
            Pool ? nullablePool = GetPool(name);                                // Получение nullable пула
            // Проверка его существования
            if (!nullablePool.HasValue) {
                Debug.LogError("Pool for " + name + " not found");
                return null;                 
            }
            Pool pool = nullablePool.Value;                                     // Получение чистого пула (структуру, не nullable)

            // Проверка пула на наличие объекта
            if (pool.UnusedObjects.Count > 0)                             
            {
                GameObject poolObject = pool.UnusedObjects[0];                  // Извлечение экземпляра объекта
                pool.UnusedObjects.RemoveAt(0);                                 // Удаление объекта из списка неиспользуемых 
                poolObject.SetActive(true);                                     // Принудительное отключаение объекта
                PoolObject[] poolObjects = poolObject.GetComponents<PoolObject>();  // Получение всех компонентов PoolObject
                for (int p = 0; p < poolObjects.Length; p++) poolObjects[p].Start();// Во всех компонентах наследуемых от PoolObject запускаем Start()
                pool.UsedObjects.Add(poolObject);                               // Перемещение объекта в используемый 
                return poolObject;
            }

            // Выход если пул полон
            if (pool.MaxObjects > 0 && pool.Count >= pool.MaxObjects) return null;

            GameObject gameObject = CreatePoolObject(name);                     // Создание экземпляра объекта
            pool.UsedObjects.Add(gameObject);                                   // Перенос в список используемых объектов
            gameObject.SetActive(true);
            return gameObject;
        }

        /// <summary>
        /// Получение ссылки на пул с заданным именем объекта
        /// </summary>
        public static Pool ? GetPool(string objectName)
        {
            for (int i = 0; i < _pools.Count; i++)                              // Перебор каждого пула с объектами
            {
                Pool pool = _pools[i];
                if (objectName != pool.ObjectName) continue;                    // Проверка имени пула с именем объекта
                return pool;
            }
            return null;
        }

        /// <summary>
        /// Создание экземпляра объекта для пула объектов
        /// </summary>
        private static GameObject CreatePoolObject(string objectName)
        {
            Pool? nullablePool = GetPool(objectName);                           // Получение nullable пула
            if (!nullablePool.HasValue) return null;                            // Проверка его существования
            Pool pool = nullablePool.Value;                                     // Получение чистого пула (структуру, не nullable)

            GameObject gameObject = Object.Instantiate(pool.OriginalGameObject);   // Создание и возврат копии объекта
            gameObject.name = objectName;
            gameObject.transform.parent = pool.Container;
            return gameObject;

        }

        /// <summary>
        /// Уничтожить пул
        /// </summary>
        /// <param name="objectName">Имя объекта</param>
        /// <param name="destroyObjects">Флаг уничтожения объектов пула</param>
        public static void DestroyPool(string objectName, bool destroyObjects = true)
        {
            Pool? nullablePool = GetPool(objectName);                           // Получение nullable пула
            // Проверка его существования
            if (!nullablePool.HasValue)
            {
                Debug.LogError("Pool for " + objectName + " not found");
                return;
            }

            Pool pool = nullablePool.Value;                                     // Получение чистого пула (структуру, не nullable)

            DestroyPool(pool, destroyObjects);
        }

        /// <summary>
        /// Уничтожиение пула
        /// </summary>
        /// <param name="pool">Объект пула</param>
        /// <param name="destroyObjects">Флаг уничтожения объектов пула</param>
        public static void DestroyPool(Pool pool, bool destroyObjects = true)
        {
            if (destroyObjects)
            {
                for (int i = 0; i < pool.UsedObjects.Count; i++)
                {
                    Object.Destroy(pool.UsedObjects[i]);
                }

                for (int i = 0; i < pool.UnusedObjects.Count; i++)
                {
                    Object.Destroy(pool.UnusedObjects[i]);
                }
            }

            _pools.Remove(pool);
        }

        /// <summary>
        /// Уничтожение всех пулов
        /// </summary>
        /// <param name="destroyObjects">Флаг уничтожения объектов пула</param>
        public static void DestroyAllPools(bool destroyObjects = true)
        {
            while (_pools.Count > 0)
            {
                DestroyPool(_pools[0], destroyObjects);
            }
        }

        /// <summary>
        /// Уничтожение неиспользуемых объектов
        /// </summary>
        /// <param name="objectName">Имя объекта</param>
        public static void DestroyUnusedObjects(string objectName)
        {
            Pool? nullablePool = GetPool(objectName);                           // Получение nullable пула
            // Проверка его существования
            if (!nullablePool.HasValue)
            {
                Debug.LogError("Pool for " + objectName + " not found");
                return;
            }

            Pool pool = nullablePool.Value;                                     // Получение чистого пула (структуру, не nullable)

            // Уничтожение неиспользуемых объектов
            for (int i = 0; i < pool.UnusedObjects.Count; i++)
            {
                Object.Destroy(pool.UnusedObjects[i]);
            }

            pool.UnusedObjects.Clear();                                         // Очистка списка
        }

        /// <summary>
        /// Пул объектов
        /// </summary>
        public struct Pool
        {
            public string ObjectName;
            public GameObject OriginalGameObject;
            public List<GameObject> UsedObjects;
            public List<GameObject> UnusedObjects;
            public Transform Container;
            public int Count { get { return UsedObjects.Count + UnusedObjects.Count;} }
            public int MaxObjects;
        
            public Pool(GameObject originalGameObject, int maxObjects)
            {
                ObjectName = originalGameObject.name;
                OriginalGameObject = originalGameObject;
                UsedObjects = new List<GameObject>();
                UnusedObjects = new List<GameObject>();
                MaxObjects = (maxObjects < 0) ? 0 : maxObjects;
                Container = new GameObject(ObjectName).transform;
                Container.parent = _objectsParent;
            }
        }
    }

    [Serializable]
    public class PoolAlreadyCreated : ApplicationException
    {
        public PoolAlreadyCreated() { }
    }
}