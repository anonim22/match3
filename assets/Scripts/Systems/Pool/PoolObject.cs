﻿using UnityEngine;

namespace GameSystems.Pool
{
    public abstract class PoolObject : MonoBehaviour
    {
        public virtual void Start()
        {

        }

        public virtual void Destroy()
        {
            PoolManager.PutObject(gameObject);
        }
    }
}